<?php

namespace App\Form;

use App\Entity\Restaurant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RestaurantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nom',
                ],
                'label' => 'Nom :'
            ])
            ->add('foodtype', TextType::class, [
                'attr' => [
                    'placeholder' => 'Type de cuisine',
                ],
                'label' => 'Type de cuisine :'
            ])
            ->add('linkPhoto', TextType::class, [
                'attr' => [
                    'placeholder' => 'Lien de la photo du restaurant',
                ],
                'label' => 'Lien de la photo du restaurant :'
            ])
            ->add('address', AddressType::class, [
                'label' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Restaurant::class,
        ]);
    }
}
