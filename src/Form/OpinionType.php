<?php

namespace App\Form;

use App\Entity\Opinion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpinionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => 'Titre',
                ],
                'label' => 'Titre :',

            ])
            ->add('message', TextType::class, [
                'attr' => [
                    'placeholder' => 'Message',
                ],
                'label' => 'Message :'
            ])
            ->add('note', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Note',
                    'min' => 0,
                    'max' => 5
                ],
                'label' => 'Note :'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Opinion::class,
        ]);
    }
}
