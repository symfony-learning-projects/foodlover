<?php

namespace App\Entity;

use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 */
class Restaurant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("all_restaurants")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le nom ne peut pas être vide.")
     * @Groups("all_restaurants")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le type de cuisine ne peut pas être vide.")
     * @Groups("all_restaurants")
     */
    private $foodtype;

    /**
     * @ORM\Column(type="string", length=2083, nullable=true)
     * @Assert\Url(message = "L'url n'est pas une url valide")
     * @Groups("all_restaurants")
     */
    private $linkPhoto;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="restaurants", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="restaurants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Opinion::class, mappedBy="restaurant", cascade={"remove"})
     */
    private $opinions;

    public function __construct()
    {
        $this->opinions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFoodtype(): ?string
    {
        return $this->foodtype;
    }

    public function setFoodtype(string $foodtype): self
    {
        $this->foodtype = $foodtype;

        return $this;
    }

    public function getLinkPhoto(): ?string
    {
        return $this->linkPhoto;
    }

    public function setLinkPhoto(?string $linkPhoto): self
    {
        $this->linkPhoto = $linkPhoto;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Opinion[]
     */
    public function getOpinions(): Collection
    {
        return $this->opinions;
    }

    public function score(): float{
        if (count($this->getOpinions()) == 0){
            return 0;
        }
        $score = 0;
        foreach ($this->getOpinions() as $value){
            $score = $score + $value->getNote();
        }
        return $score/(count($this->getOpinions()));
    }

    public function addOpinion(Opinion $opinion): self
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions[] = $opinion;
            $opinion->setRestaurant($this);
        }

        return $this;
    }

    public function removeOpinion(Opinion $opinion): self
    {
        if ($this->opinions->contains($opinion)) {
            $this->opinions->removeElement($opinion);
            // set the owning side to null (unless already changed)
            if ($opinion->getRestaurant() === $this) {
                $opinion->setRestaurant(null);
            }
        }

        return $this;
    }
}
