<?php


namespace App\Controller;

use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class AppController extends AbstractController{

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(RestaurantRepository $restaurantRepository): Response{

        return $this->render('index.html.twig', [
            'restaurants' => $restaurantRepository->findAllOpinions(),
        ]);
    }
}