<?php

namespace App\Controller;

use App\Entity\Restaurant;
use App\Form\RestaurantType;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/espace-restaurateur")
 * @IsGranted("ROLE_RESTORER")
 */
class RestorerSpaceController extends AbstractController
{
    /**
     * @Route("/", name="restorer_space", methods={"GET", "POST"})
     */
    public function index(Request $request, RestaurantRepository $restaurantRepository): Response{

        $restaurant = new Restaurant();
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);
        $badform = false;

        if ($form->isSubmitted() && $form->isValid()) {

            $restaurant->setUser($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($restaurant);
            $entityManager->flush();

            $restaurant = new Restaurant();
            $form = $this->createForm(RestaurantType::class, $restaurant);
        }

        if ($form->isSubmitted() && $form->getErrors()){
            $badform = true;
        }

        return $this->render('restorer_space/index.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form->createView(),
            'badform' => $badform,
            'myRestaurants' => $restaurantRepository->findAllByUser($this->getUser()),
        ]);
    }

    /**
     * @Route("/restaurant/{id}/edit", name="restaurant_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Restaurant $restaurant): Response
    {
        $form = $this->createForm(RestaurantType::class, $restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('restorer_space');
        }

        return $this->render('restorer_space/edit.html.twig', [
            'restaurant' => $restaurant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/restaurant/{id}", name="restaurant_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Restaurant $restaurant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$restaurant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($restaurant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('restorer_space');
    }
}

